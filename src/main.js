import Vue from 'vue'
import VueOffline from 'vue-offline'
import VueScrollTo from 'vue-scrollto'
import axios from 'axios'
import vuedayjs from './vue-dayjs';

import './registerServiceWorker'

import './assets/scss/app.scss'
import './buefy';
import "vue-material-design-icons/styles.css"

Vue.config.productionTip = false

Vue.use(vuedayjs, {
    lang: 'uk'
});
Vue.use(VueScrollTo);
Vue.use(VueOffline, {
    mixin: false
});

import App from './App.vue'

Vue.prototype.localStorage = window.localStorage;
if(window.localStorage.getItem('api_url') == undefined) window.localStorage.setItem('api_url', 'https://backend-school.in-story.org');
window.setUrl = function (url) {
    window.localStorage.setItem('api_url', url);
    api.api_url = url;
}
var api = {
    axios: axios,
    domain: "https://elitedomashka.gitlab.io".match(/https?:\/\/(?:[-\w]+\.)?([-\w]+)\.\w+(?:\.\w+)?\/?.*/i)[1],
    api_url: window.localStorage.getItem('api_url'),
    callApi(method, data, callback){
        return this.axios.get(this.api_url+'/api/method/'+method, {
            params: data
        })
            .then(callback)
            .catch(function (error) {
                // eslint-disable-next-line
                console.log(error);
            });
    },
};
window.api = api;


import router from './router'

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
