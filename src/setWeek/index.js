// import { MS, Y, D, W } from '../../constant'

export default (o, c) => {
    const proto = c.prototype
    proto.setWeek = function (week) {
        return this.add((week - this.week())*7, 'day');
    }
}
