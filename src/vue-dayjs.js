// import Vue from 'vue'
import dayjs from 'dayjs'
import locale from 'dayjs/locale/uk';
dayjs.locale(locale);
import weekOfYear from 'dayjs/plugin/weekOfYear'
// import setWeek from './setWeek/index'

const InvalidReturn = '';


export default {
 install(Vue, options = {}){
     dayjs.locale(options.lang);
     dayjs.extend(weekOfYear);
     // dayjs.extend(setWeek);
     Vue.prototype.$dayjs = dayjs;
     Vue.filter('dayjs', (value, method, ...params) => {
         let d = dayjs(value);
         if (!d.isValid()) return InvalidReturn;
         if (method) {
             return d[method].apply(d, params);
         }
     });
 }

}

