# EliteDomashka Frontend

Устновка зависмостей: `yarn` или `npm install`  
Сборка: `yarn run build` или `npm run build`  
Запуск в рижиме отладки(dev): `yarn run serve` или `npm run serve`

## Установка
Собранный проект в архиве можете загрузить с [Gitlab CI](https://gitlab.com/EliteDomashka/elitedomashka.gitlab.io/-/jobs/artifacts/master/download?job=pages).   
Или же собрать самостоятельно предварительно установив [Node.js](https://nodejs.org/en/download/) и пакетный менеджер (npm/yarn) 
### Cборка
Для начала необходимо клонировать проект `$ git clone https://gitlab.com/EliteDomashka/elitedomashka.gitlab.io.git` или же просто загрузить и распоковать [архив](https://gitlab.com/EliteDomashka/elitedomashka.gitlab.io/-/archive/master/elitedomashka.gitlab.io-master.zip).  
**ОБЯЗАТЕЛЬНО** отредактируйте [src/main.js](https://gitlab.com/EliteDomashka/elitedomashka.gitlab.io/blob/master/src/main.js#L37) и измените api_url на свой URL [backend'a](https://gitlab.com/EliteDomashka/backend) 
### Публичный доступ 
#### ZEIT Now
Вы можете захостить статичную часть на https://zeit.co/now, в придачу получить домен вида subdomain.now.sh (пример [elitedomashka.now.sh](https://elitedomashka.now.sh)).

Для этого необходимо:
1. Устновить [now-cli](https://github.com/zeit/now-cli)
2. Выполнить инструкции now-cli прописав `now`(в терминале) в папке проекта
